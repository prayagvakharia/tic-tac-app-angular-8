import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-squarebox',
  template: `
    <button>{{value}}</button>
  `,
  styles: ['button { width: 100%; height: 100%; font-size: 4.5em !important; }']
})
export class SquareboxComponent {
  @Input() value: 'X' | 'O';
}
