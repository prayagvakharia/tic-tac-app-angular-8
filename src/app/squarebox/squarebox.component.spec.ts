import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SquareboxComponent } from './squarebox.component';

describe('SquareboxComponent', () => {
  let component: SquareboxComponent;
  let fixture: ComponentFixture<SquareboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SquareboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SquareboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
